FROM scottyhardy/docker-wine:stable-8.0

ENV RUN_AS_ROOT=yes

WORKDIR /root/.wine/drive_c/Program Files/Studio 2.0/

RUN wget https://github.com/sibprogrammer/xq/releases/download/v1.1.3/xq_1.1.3_linux_amd64.tar.gz -O xq.tar.gz && \
    tar xvf xq.tar.gz && \
    mkdir -p /usr/bin/local && \
    mv xq /usr/local/bin && \
    rm -f xq.tar.gz LICENSE README.md && \
    wget https://github.com/mikefarah/yq/releases/download/v4.31.1/yq_linux_amd64 -O yq && \
    chmod +x yq && \
    mv yq /usr/local/bin/


ADD . . 

WORKDIR /root/
