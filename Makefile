


IMAGE_REPO=registry.gitlab.com/ttblt-oss/docker
IMAGE_NAME=bricklink-studio
IMAGE_VERSION=2.23.7.3-1
IMAGE_FULL_NAME=$(IMAGE_REPO)/$(IMAGE_NAME):$(IMAGE_VERSION)
STUDIO_FOLDER=

build-local:
	docker build -t $(IMAGE_FULL_NAME) -f Dockerfile '$(STUDIO_FOLDER)'

push:
	docker push $(IMAGE_FULL_NAME)
